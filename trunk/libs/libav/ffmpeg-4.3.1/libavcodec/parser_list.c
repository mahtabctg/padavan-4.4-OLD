static const AVCodecParser * const parser_list[] = {
    &ff_aac_parser,
    &ff_ac3_parser,
    &ff_dca_parser,
    &ff_flac_parser,
    &ff_mpegaudio_parser,
    NULL };
